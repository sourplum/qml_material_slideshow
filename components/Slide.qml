import QtQuick 2.5
import Material 0.1

Page {
    id: slide

    backAction: Action {
        name: "Back"
        iconName: "navigation/arrow_back"
        onTriggered: {
            goToPreviousSlide();
        }
        visible: (pageStack.depth > 1)
    }

    Keys.onSpacePressed: goToNextSlide()
    Keys.onRightPressed: goToNextSlide()
    Keys.onDownPressed: goToNextSlide()
    Keys.onLeftPressed: goToPreviousSlide()
    Keys.onUpPressed: goToPreviousSlide()
    Keys.onEscapePressed: Qt.quit()
    Keys.onPressed: {
        if (event.key === Qt.Key_F) {
            toggleFullscreen();
        }
        else if (event.key === Qt.Key_Backspace) {
            goToPreviousSlide();
        }
    }
}
