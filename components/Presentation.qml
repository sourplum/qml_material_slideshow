import QtQuick 2.5
import QtQuick.Window 2.2
import Material 0.1

ApplicationWindow {
    id: presentation

    width: Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight

    property int currentSlide: 0;
    property int lastSlide: slides.length - 1;
    property bool isFullscreen: false;
    property variant slides: [];

    function goToPreviousSlide() {
        if (pageStack.depth > 1 ) {
            --currentSlide
            pageStack.pop()
        }
    }

    function goToNextSlide() {
        if (pageStack.depth < slides.length )
        {
            pageStack.push( slide(++currentSlide) )
        }
    }

    function slide(num) {
        return Qt.resolvedUrl("../slides/" + slides[num] +  "/slide.qml")
    }

    function toggleFullscreen()
    {
        isFullscreen = !isFullscreen;
        if (isFullscreen) {
            mainWindow.showFullScreen();
        } else {
            mainWindow.showMaximized();
        }
    }
}
