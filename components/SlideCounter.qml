import QtQuick 2.5
import Material 0.1

ProgressCircle {
    id: slideCounter
    property string textColor: "black"

    anchors {
        right: parent.right
        bottom: parent.bottom
        margins: Units.dp(12)
    }
    
    width: Units.dp(64)
    height: Units.dp(64)
    indeterminate: false
    minimumValue: (currentSlide > 0) ? currentSlide - 1 : 0
    maximumValue: lastSlide - 1

    SequentialAnimation on value {
        running: true

        NumberAnimation {
            duration: 1000
            from: minimumValue
            to: currentSlide
        }
    }
    
    Label {
        anchors.centerIn: parent
        text: currentSlide
        color: textColor
    }
}
