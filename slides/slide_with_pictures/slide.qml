import QtQuick 2.5
import QtQuick.Layouts 1.1
import Material 0.1
import Material.ListItems 0.1 as ListItem
import "../../components"

Slide {
    id: page
    title: "Slide with pictures"

    SlideView {
        id: slideView

        Flickable {
            anchors.fill: parent

            contentWidth: columnlayout.width
            contentHeight: columnlayout.height
            ColumnLayout {
                id: columnlayout
                anchors {
                    left: parent.left
                    top: parent.top
                    margins: Units.dp(34)
                }
                spacing: Units.dp(16)

                Label {
                    font.family: "Roboto"
                    text: "Qt5 Overview"
                    font.pixelSize: Units.dp(34)

                    anchors {
                        margins: Units.dp(34)
                        horizontalCenter: parent.horizontalCenter
                    }
                }
                Image {
                    fillMode: Image.PreserveAspectCrop
                    source: Qt.resolvedUrl("assets/qt5_overview.png")
                }
                Label {
                    font.family: "Roboto"
                    text: "Papyros OS"
                    font.pixelSize: Units.dp(34)

                    anchors {
                        margins: Units.dp(34)
                        horizontalCenter: parent.horizontalCenter
                    }
                }
                Image {
                    fillMode: Image.PreserveAspectCrop
                    source: Qt.resolvedUrl("assets/papyros-laptop.png")
                }
            }
        }
    }
}
